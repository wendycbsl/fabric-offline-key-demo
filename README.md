Please put this folder under fabric-samples

First set up the test network, deploy the 'basic' chaincode

Then we can call `enrollAdmin` script to get the keys for admin

To generate the private key, use prime256v1 with the following command:

```bash
openssl ecparam -name prime256v1 -genkey -noout -out test.key
```

Then generate the csr (Certificate Signing Request) which contains the public key

```bash
openssl req -new -key test.key -out test.csr
```

While generating csr, note that the common name must be the same with enrollment ID, then set it in the `USERNAME` environmental variable which will be used by the scripts)

Then we can call `registerUser` to get back the certificates signed by ca (which will be put in `test.pem`) and we use the following command to check cert infos:

```bash
openssl x509 -in test.pem -text -noout
```

Finally, we can call the `invoke` and `query` script to interact with chaincode

