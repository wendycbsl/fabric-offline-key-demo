/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Wallets, X509Identity } from 'fabric-network';
import * as FabricCAServices from 'fabric-ca-client';
import * as path from 'path';
import * as fs from 'fs';

async function main() {
    const USERNAME = process.env["USERNAME"];

    try {
        // load the network configuration
        const ccpPath = path.resolve(__dirname, '..', '..','test-network','organizations','peerOrganizations','org1.example.com', 'connection-org1.json');
        let ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // Create a new CA client for interacting with the CA.
        const caURL = ccp.certificateAuthorities['ca.org1.example.com'].url;
        const ca = new FabricCAServices(caURL);

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the admin user.
        const adminIdentity = await wallet.get('admin');
        if (!adminIdentity) {
            console.log('An identity for the admin user "admin" does not exist in the wallet');
            console.log('Run the enrollAdmin.ts application before retrying');
            return;
        }

        // build a user object for authenticating with the CA
        const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type);
        const adminUser = await provider.getUserContext(adminIdentity, 'admin');

        // get the csr
        const csr = fs.readFileSync('./test.csr', 'utf8');

        // Register the user, enroll the user, write the certificate of the user
        const secret = await ca.register({ affiliation: 'org1.department1', enrollmentID: USERNAME, role: 'client' }, adminUser);
        console.log(`user secret: ${secret}`);
        const enrollment = await ca.enroll({ enrollmentID: USERNAME, enrollmentSecret: secret, csr: csr });
        fs.writeFileSync('./test.pem', enrollment.certificate);
        
        console.log(`Successfully registered and enrolled admin user ${USERNAME} and imported it into the wallet`);

    } catch (error) {
        console.error(`Failed to register user ${USERNAME}: ${error}`);
        process.exit(1);
    }
}

main();
