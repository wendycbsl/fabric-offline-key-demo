/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Gateway, Wallets } from 'fabric-network';
import { User } from 'fabric-common';
import * as path from 'path';
import * as fs from 'fs';
import { createHash } from 'crypto';
const elliptic = require('elliptic');
const { KEYUTIL } = require('jsrsasign');

async function main() {
    const USERNAME = process.env["USERNAME"];
    try {
        // load the network configuration
        const ccpPath = path.resolve(__dirname, '..', '..','test-network','organizations','peerOrganizations','org1.example.com', 'connection-org1.json');
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the admin user.
        const adminIdentity = await wallet.get('admin');
        if (!adminIdentity) {
            console.log('An identity for the admin user "admin" does not exist in the wallet');
            console.log('Run the enrollAdmin.ts application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: "admin", discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // set up user
        const channel = network.getChannel();
        const cert = fs.readFileSync("./test.pem", "utf8");
        // need to put cert in user
        const user = User.createUser(USERNAME, null, "Org1MSP", cert);
        const idx = channel.client.newIdentityContext(user);

        // build proposal & calculate the hash
        const endorsement = channel.newEndorsement('basic');
        // const build_options = { fcn: 'InitLedger', args: [] };
        const build_options = { fcn: 'CreateAsset', args: ['asset12', 'yellow', '5', 'Tom', '1300'] };
        const proposalBytes = endorsement.build(idx, build_options);
        const hash = createHash("sha256");
        const endorsement_digest = hash.update(proposalBytes).digest('hex');
        
        const endorsement_sig = sign(endorsement_digest);
        endorsement.sign(endorsement_sig);

        // send proposal request 
        const endorse_request = {
            targets: channel.getEndorsers(),
            requestTimeout: 30000
        };
        await endorsement.send(endorse_request);

        // commit the endorsement
        // sign the commit 
        const commit = channel.newCommit("basic");
        // note that this call will throw error if no valid endorsement response found
        const commitBytes = commit.build(idx, { endorsement: endorsement });
        const hash_1 = createHash("sha256");
        const commit_digest = hash_1.update(commitBytes).digest('hex');
        const commit_sig = sign(commit_digest);
        commit.sign(commit_sig);

        // send the commit
        const commit_send_request = {
            targets: channel.getCommitters(),
            requestTimeout: 30000
        };
        await commit.send(commit_send_request);

        // Submit the specified transaction.
        console.log(`Transaction has been submitted`);


    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        console.error(`Stack trace: \n`, error.stack);
        process.exit(1);
    }
}

// assume this function will be executed on the mobile device
function sign(digest: string) {
    const privateKeyPEM = fs.readFileSync("./test.key", "utf8");
    const { prvKeyHex } = KEYUTIL.getKey(privateKeyPEM); // convert the pem encoded key to hex encoded private key

    // calculate the signature
    const EC = elliptic.ec;
    const ecdsaCurve = elliptic.curves['p256'];
    const ecdsa = new EC(ecdsaCurve);
    const signKey = ecdsa.keyFromPrivate(prvKeyHex, 'hex');
    const sig = ecdsa.sign(Buffer.from(digest, 'hex'), signKey);

    // prevent malleability
    // s of the signature must be smaller than half the order of pub key
    // see https://www.npmjs.com/advisories/1547
    // if don't do this, peer doesn't consider this signature secure
    const halfOrder = elliptic.curves.p256.n.shrn(1);
    if (sig.s.cmp(halfOrder) === 1) { 
        const bigNum = elliptic.curves.p256.n;
        sig.s = bigNum.sub(sig.s);
    }

    // now we have the signature, send the signature back to server
    return Buffer.from(sig.toDER());
}

main();
